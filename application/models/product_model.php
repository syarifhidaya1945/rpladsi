<?php
class Product_model extends CI_Model{
    public $id_product;
    public $nama_product;
    public $harga;
    public $tersedia;
    public $foto_url;
    public $create_at;
    public $update_at;

    public function getProduct()
    {
        $this->load->database();
        $products = $this->db->get("produk");
        $result = $products->result();
        return json_encode ($result);
    }
}