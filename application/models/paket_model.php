<?php
class paket_model extends CI_Model{
    public $id_paket;
    public $nama_paket;
    public $tersedia;
    public $create_at;
    public $update_at;

    public function getpaket()
    {
        $this->load->database();
        $paket = $this->db->get("paket");
        $result = $paket->result();
        return json_encode($result);
    }
}
