<?php
class detailpaket_model extends CI_Model{
    public $id_produk;
    public $id_paket;
    public $create_at;
    public $update_at;

    public function getdetailpaket()
    {
        $this->load->database();
        $detailpaket = $this->db->get("detailpaket");
        $result = $detailpaket->result();
        return json_encode($result);
    }
}
